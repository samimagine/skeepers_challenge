import { useEffect, useState } from 'react';

function UsersList() {
  const [users, setUsers] = useState([]);

  const ENDPOINT = 'https://randomuser.me/api/?results=20';

  useEffect(() => {
    fetch(ENDPOINT)
      .then((response) => response.json())
      .then((data) => {
        setUsers(data.results);
      });
  }, [setUsers]);

  return (
    <div>
        {console.log(users)}
     <ul>
        {users.map((user, index) => (
          <li key={index} id={index}>{user.name.first}</li>
        ))}
      </ul>
    </div>
  );
}

export default UsersList;
